import React from "react";
import styled from "styled-components";

const StyledLabel = styled.label`
  font-weight: bold;
  display: block;
  margin-bottom: 8px;
  font-size: 16px;
  color: var(--normal-text-color)
`;

const Label = ({ htmlFor, children }) => {
  return <StyledLabel htmlFor={htmlFor}>{children}</StyledLabel>;
};

export default Label;