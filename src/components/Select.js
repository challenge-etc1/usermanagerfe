import React from "react";
import styled from "styled-components";

const SelectContainer = styled.div`
  position: relative;
  width: 100%;
`;

const SelectElement = styled.select`
  width: 100%;
  padding: 12px;
  border-radius: 4px;
  border: 1px solid #ced4da;
  font-size: 16px;
  appearance: none;
  background: #fff url("data:image/svg+xml;charset=US-ASCII,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%204%205'><path%20fill='%23666'%20d='M2%200l2%202-2%202-2-2%202-2z'/></svg>") no-repeat right 10px center;
  background-size: 12px;
`;

const Select = ({ id, name, required, value, children, onChange }) => {
  return (
    <SelectContainer>
      <SelectElement id={id} name={name} required={required} value={value} onChange={onChange}>
        {children}
      </SelectElement>
    </SelectContainer>
  );
};

export default Select;