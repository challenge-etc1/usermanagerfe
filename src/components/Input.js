import React from "react";
import styled from "styled-components";

const StyledInput = styled.input`
  width: 98%;
  padding: 12px 0;
  border-radius: 4px;
  border: 1px solid #ced4da;
  font-size: 16px;
  margin-bottom: 16px;
`;

const Input = ({ id, name, type, autoComplete, required, placeholder, pattern, min, max , onChange}) => {
  return (
    <StyledInput
      id={id}
      name={name}
      type={type}
      autoComplete={autoComplete}
      required={required}
      placeholder={placeholder}
      pattern={pattern}
      min={min}
      max={max}
      onChange={onChange}
    />
  );
};

export default Input;