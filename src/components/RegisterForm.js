import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import Label from "./Label";
import Input from "./Input";
import Select from "./Select";
import Button from "./Button";
import axios from "axios";
import Swal from "sweetalert2";

const FormContainer = styled.div`
  display: flex;
  min-height: 100vh;
  align-items: center;
  justify-content: center;
  background-color: #f0f0f0;
`;

const FormWrapper = styled.div`
  width: 100%;
  max-width: 26rem;
  margin: 0 auto;
  padding: 24px;
  background-color: #ffffff;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
`;

const FormTitle = styled.h1`
  text-align: center;
  font-size: 24px;
  font-weight: bold;
  color: var(--normal-text-color);
  margin-bottom: 16px;
`;

const FormSubtitle = styled.p`
  text-align: center;
  font-size: 14px;
  color: var(--subtitle-text-color);
  margin-bottom: 24px;
`;

const TextNote = styled.p`
  font-size: 14px;
  color: var(--subtitle-text-color);
  margin-top: 4px;
`;

const BackToLogin = styled.div`
  text-align: center;
  margin-top: 16px;

  a {
    font-weight: bold;
    color: var(--primary-color);
    text-decoration: none;

    &:hover {
      color: var(--secondary-color);
    }
  }
`;

const RegisterForm = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
      username: '',
      phoneNumber: '',
      age: '',
      gender: '',
      password: ''
    });
  
    const handleChange = (event) => {
      const { name, value } = event.target;
      setFormData(prevState => ({ ...prevState, [name]: value }));
    };
  
    const handleSubmit = async (event) => {
      event.preventDefault();
      try {
        const response = await axios.post('http://localhost:8080/users/register', formData);
        navigate('/login');
      } catch (error) {
        console.error('Registration failed:', error.response ? error.response : error.message);
        Swal.fire({
          icon: 'error',
          title: 'Authentication Failed',
          text: 'Please try again later.',
          confirmButtonText: 'OK'
        });
      }
    };  

    return (
        <FormContainer>
            <FormWrapper>
                <FormTitle>Register</FormTitle>
                <FormSubtitle>Fill out the form below to create a new account.</FormSubtitle>
                <form onSubmit={handleSubmit}>
                    <div>
                        <Label htmlFor="username">Username</Label>
                        <Input id="username" name="username" placeholder="Enter your username" pattern="^[a-zA-Z]+$" required onChange={handleChange} value={formData.username} />
                        <TextNote>Username must contain only letters and be unique.</TextNote>
                    </div>
                    <div>
                        <Label htmlFor="phone">Phone</Label>
                        <Input id="phone" name="phone" placeholder="0000-0000" pattern="\d{4}-\d{4}" required onChange={handleChange} value={formData.phoneNumber} />
                    </div>
                    <div>
                        <Label htmlFor="age">Age</Label>
                        <Input id="age" name="age" type="number" min="18" max="120" placeholder="Enter your age" required onChange={handleChange} value={formData.age} />
                    </div>
                    <div>
                        <Label htmlFor="gender">Gender</Label>
                        <Select id="gender" name="gender" required onChange={handleChange} value={formData.gender}>
                            <option value="">Choose your gender...</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Other</option>
                        </Select>
                    </div>
                    <div>
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            name="password"
                            type="password"
                            placeholder="Enter your password"
                            pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,12}$"
                            required
                            onChange={handleChange}
                            value={formData.password}
                        />
                        <TextNote>Password must be 8-12 characters long and contain at least one uppercase letter, one lowercase letter, and one number.</TextNote>
                    </div>
                    <Button type="submit">Register</Button>
                </form>
                <BackToLogin>
                    <Link to="/login">Back to Login</Link>
                </BackToLogin>
            </FormWrapper>
        </FormContainer>
    );
};

export default RegisterForm;