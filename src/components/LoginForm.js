import React, { useState } from "react";
import styled from "styled-components";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import Label from "./Label";
import Input from "./Input";
import Button from "./Button";
import Swal from "sweetalert2";

const FormContainer = styled.div`
  display: flex;
  min-height: 100vh;
  align-items: center;
  justify-content: center;
  background-color: #f0f0f0;
`;

const FormWrapper = styled.div`
  width: 100%;
  max-width: 26rem;
  margin: 0 auto;
  padding: 24px;
  background-color: #ffffff;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
`;

const FormTitle = styled.h2`
  margin-top: 24px;
  text-align: center;
  font-size: 24px;
  font-weight: bold;
  color: #333;
`;

const FormSubtitle = styled.p`
  margin-top: 8px;
  text-align: center;
  font-size: 14px;
  color: #666;

  a {
    font-weight: bold;
    color: var(--primary-color);

    &:hover {
      color: var(--secondary-color);
    }
  }
`;

const LoginForm = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
      username: '',
      password: ''
    });
  
    const handleChange = (event) => {
      const { name, value } = event.target;
      setFormData(prevState => ({ ...prevState, [name]: value }));
    };
  
    const handleSubmit = async (event) => {
      event.preventDefault();
      try {
        const response = await axios.post('http://localhost:8080/users/login', formData, {
          withCredentials: true
        });
        // const token = response.data.token;
        // document.cookie = `authToken=${token}; path=/; domain=localhost; max-age=3600;`;
  
        
        window.location.href = `http://localhost:3005/`;
      } catch (error) {
        console.error('Login failed:', error.response ? error.response : error.message);
        Swal.fire({
          icon: 'error',
          title: 'Authentication Failed',
          text: 'Please try again later.',
          confirmButtonText: 'OK'
        });
      }
    };  

  return (
    <FormContainer>
      <FormWrapper>
        <FormTitle>Sign in to your account</FormTitle>
        <FormSubtitle>
          Or{" "}
          <Link to="/register" prefetch="false">
            register for a new account
          </Link>
        </FormSubtitle>
        <form onSubmit={handleSubmit}>
          <div>
            <Label htmlFor="username">Username</Label>
            <Input id="username" name="username" type="text" autoComplete="username" required onChange={handleChange} value={formData.username} />
          </div>
          <div>
            <Label htmlFor="password">Password</Label>
            <Input id="password" name="password" type="password" autoComplete="current-password" required onChange={handleChange} value={formData.password} />
          </div>
          <div>
            <Button type="submit">Sign in</Button>
          </div>
        </form>
      </FormWrapper>
    </FormContainer>
  );
};

export default LoginForm;