import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,opsz,wght@0,6..12,200..1000;1,6..12,200..1000&display=swap');

  :root {
    --primary-color: #0070f3;
    --secondary-color: #005bb5;
    --error-color: red;
    --normal-text-color: #000000;
    --subtitle-text-color: #666666; /* Nuevo color agregado */
  }

  body {
    font-family: 'Nunito', sans-serif;
    color: var(--primary-color);
    background-color: #f0f0f0;
    margin: 0;
    padding: 0;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: 'Nunito', sans-serif;
  }
`;

export default GlobalStyle;